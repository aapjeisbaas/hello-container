Basic docker instance that listens on port 8080 for http request and returns status 200.

I use it to keep infra from respawning as this is a placeholder instace befor the real app is deployed.

### Build it:
```
docker build -t hello .
```

### Run it:
```
docker run -p 8080:8080 hello
```

### Test it:
```
curl 127.0.0.1:8080/what/is/my/path
```

![Google Analytics](https://www.google-analytics.com/collect?v=1&tid=UA-48206675-1&cid=555&aip=1&t=event&ec=repo&ea=view&dp=gitlab%2Fhello-container%2FREADME.md&dt=hello-container)

