from bottle import Bottle

app = Bottle()

@app.route('<mypath:path>')
def hello(mypath):
    return 'Your path is: %s\n' % mypath